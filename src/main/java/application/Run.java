package application;

import algorithm.CompositeTextDensity;
import algorithm.Density;
import algorithm.TextDensity;
import document.*;
import document.BrowserHeadless;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.openqa.selenium.WebDriver;

import java.util.HashMap;
import java.util.Map;

public class Run {
    // chạy thuật toán kiểm tra các điều kiện và in ra text
    public static void run(String url, int w, WebDriver driver){
        String result="";
        Map<Element, Double> mapDensityGlobal = new HashMap<>();
        Element element=null;
        if(url.contains("batdongsan.com.vn")){
            driver.get(url);
            Document document = Doc.getDocumentByString(driver.getPageSource());
            result+=document.title()+"\n";
            element=Doc.getBodyDocument(document);
        }
        else if(url.contains("shoppe.vn")){
            Document document = Doc.getDocumentByString(driver.getPageSource());
            result+=document.title()+"\n";
            element=Doc.getBodyDocument(document);
        } else{
            result+=Doc.getDocumentByUrl(url).title()+"\n";
            element= Doc.getBodyDocument(Doc.getDocumentByUrl(url));
        }

        TextDensity textDensity = new TextDensity();
        CompositeTextDensity compositeTextDensity = new CompositeTextDensity();
        long cb = compositeTextDensity.countChar(element);
        long lcb= compositeTextDensity.countLinkChar(element);
        compositeTextDensity.setLcb(lcb);
        compositeTextDensity.setLcb(cb);
        //mapDensityGlobal=textDensity.ComputingDensity(element);
        Density density = compositeTextDensity;
        //Density density = textDensity;
        mapDensityGlobal=density.ComputingDensity(element);

        double thres1 = density.threshold(element);
        double thres2 = density.getDensitySumOfElement(element);
        double thres = thres1>thres2 ? thres1:thres2;
        for(Map.Entry<Element, Double> ele : mapDensityGlobal.entrySet()){
            if(ele.getValue()>thres*w){
                if(!ClearElement.clear1(ele.getKey()) && !ClearElement.clear2(ele.getKey()) && !ClearElement.clear3(ele.getKey())) {
                    result+=Doc.getTextInTag(ele.getKey())+"\n";
                }
            }
        }

        result+=AddElement.addElementTable(element)+"\n";

        System.out.println(Doc.normalize(result));
    }
}
