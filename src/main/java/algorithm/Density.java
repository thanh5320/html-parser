package algorithm;

import org.jsoup.nodes.Element;

import java.util.Map;

// interface tất cả các thuật toán phải triển khai
public interface Density {

    // lấy về số 'tag+1' của 1 element
    public long countTagPlus(Element element);

    // lấy về số tag của 1 element
    public long countTag(Element element);

    // lấy về số ký tự của 1 thẻ
    public long countChar(Element element);

    // tính mật độ văn bản của 1 thẻ: thường hoặc tổng hợp
    public double getDensityOfElement(Element element);

    // trả về map chứa mật độ văn bản của tất cả các thẻ trong element
    public Map<Element, Double> ComputingDensity(Element element);

    //tính tổng mật độ văn bản của 1 thẻ
    public double getDensitySumOfElement(Element element);

    //  trả về tổng mật độ văn bản của tất cả các thẻ
    public Map<Element, Double> ComputingDensitySum(Element element);

    // trả về thẻ có mật độ văn bản lớn nhất
    public Element maxElementDensity(Element element);

    // trả về ngưỡng của mật độ văn bản để quyết định xem văn bản nào sẽ được in ra
    public double threshold(Element element);
}
