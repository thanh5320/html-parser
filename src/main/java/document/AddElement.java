package document;


import org.jsoup.nodes.Element;

import java.util.HashSet;
import java.util.Locale;
import java.util.Set;

public class AddElement {
    public static String addElementTable(Element element){
        Set<String> str= new HashSet<>();
        for(Element e: element.getAllElements()){
            if(e.tagName().toLowerCase().equals("table") ||
                    e.id().toLowerCase().contains("table") ||
                    e.className().toLowerCase().contains("table")){
                        str.add(e.text());
            }
        }

        for(Element e: element.getAllElements()){
            if(e.tagName().toLowerCase().equals("tabbe") ||
                    e.id().toLowerCase().contains("tabbe") ||
                    e.className().toLowerCase().contains("tabbe")){
                str.add(e.text());
            }
        }

        String text="";
        for(String s: str){
            text+=s;
        }
        return text;
    }
}
