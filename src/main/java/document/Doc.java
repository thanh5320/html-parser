package document;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.nodes.Node;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class Doc {
    // trả về document theo url
    public static Document getDocumentByUrl(String url){
        Document document = null;
        try {
            document = Jsoup.connect(url).get();
        } catch (IOException e) {
            document=getDocumentByString(BrowserHeadless.getHTML(url));
            //System.out.println("error: don't get html from url");
        }
        return document;
    }

    // trả về document từ string
    public static Document getDocumentByString(String str){
        Document doc = null;
        doc=Jsoup.parse(str);
        return doc;
    }

    // trả về body của document
    public static Element getBodyDocument(Document doc){
        //doc.body();
        return doc.body();
    }

    // lấy text từ 1 tag: chỉ lấy text tại chính thẻ đó thẻ con của nó không lấy
    public static String getTextInTag(Element element) {
        List<Node> listChildNodes = element.childNodes();
        List<String> listContent = new ArrayList<String>();

        for(Node node : listChildNodes) {
            String text = node.outerHtml();
            if(checkHtmlChildNode(text)) {
                listContent.add(text);
            }
        }

        String content = "";
        for(String s : listContent) {
            content = content + "\n" + s;
        }

        content = normalize(content);
        return content;
    }

    // kiểm trả thẻ con trong text của 1 thẻ
    public static boolean checkHtmlChildNode(String text) {
        if(text.contains("</") || text.contains("<img") || text.contains("<input") || text.contains("<!--")) {
            return false;
        }

        return true;
    }

    // chuẩn hóa lại text để in ra
    public static String normalize(String text) {
        text=text.trim();
        int lens = text.length();

        int start = 0;
        for(int i=0; i<lens; i++) { // find start
            if(text.charAt(i) != ' ' && text.charAt(i) != '\n') {
                start = i;
                break;
            }
        }

        int last = lens - 1;
        for(int i=lens-1; i>=0; i--) { // fine end
            if(text.charAt(i) != ' ' && text.charAt(i) != '\n') {
                last = i;
                break;
            }
        }

        String newText = text.substring(start, last+1);

        // remove rag <br>
        newText = newText.replace("<br>", "");

        // remove space if duplicate
        while(newText.contains("  ")) {
            newText = newText.replace("  ", " ");
        }

        while(newText.contains("&nbsp;")) {
            newText = newText.replace("&nbsp;", "");
        }

        // remove space after \n
        while(newText.contains("\n ")) {
            newText = newText.replace("\n ", "\n");
        }

        // remove \n if duplicate
        while(newText.contains("\n\n")) {
            newText = newText.replace("\n\n", "\n");
        }

        newText.trim();
        while (newText.endsWith("\n")) newText=newText.substring(0, newText.length()-1);
        newText.trim();
        while (newText.startsWith("\n")) newText=newText.substring(1);

        newText=newText.trim();
        return newText;
    }
}
