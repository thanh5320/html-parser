package document;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;

import java.io.IOException;

public class Browser {
    public static WebDriver getWebDriver(){
        String current="";
        try {
            current = new java.io.File( "." ).getCanonicalPath();
        } catch (IOException e) {
            e.printStackTrace();
        }
        System.setProperty("webdriver.chrome.driver",current+"\\resources\\chromedriver.exe");
        ChromeOptions options = new ChromeOptions();
        WebDriver driver = new ChromeDriver(options);
        return driver;
    }
    public static String getHTML(String url){
        WebDriver driver = getWebDriver();
        driver.get(url);
        String rs = driver.getPageSource();
        return rs;
    }
}
